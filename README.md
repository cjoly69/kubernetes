# kubernetes

## atelier Kubernetes

<-- voir vimtutor pour apprendre vim en installant sudo yum install vim -y -->

pour démarrer la démo je pars d'un vagrantfile minikube de Dirane Tafen (https://github.com/diranetafen/cursus-devops/tree/master/vagrant)
On travaille en ssh
```
vagrant ssh
minikube start --driver=none
```
Vérification de la création du node
```
kubectl get node
```
création d'un répertoire de travail
```
mkdir -p Kubernetes-training/tp-2 && cd Kubernetes-training/tp-2
```
création du premier manifest (descrition de l'objet)
```
vi pod.yml
```

Lancement du poste
```
kubectl apply -f pod.yml
```

verification de la création du pode
```
kubectl get po

kubectl describe pod simple-webapp-color
```
Pour connaitre la liste des ressources et leurs alias : 
```
kubectl api-resources    
```
ip a pour voir quelle est l'ip machine : 192.168...

expostion de l'appli pour la consommer
```
kubectl port-forward simple-webapp-color 8080:8080 --address 0.0.0.0
```

test dans le navigateur http://{mon_IP}:8080/

on supprime le pod, l'objet qui a été créé par le fichier yaml
```
kubectl delete -f pod.yml
```
nouvel objet deploiement nginxen version 1.18.0 
```
vi nginx-deployment.yml
```
```
kubectl apply -f nginx-deployment.yml
```
verification :
```
kubectl describe deployment nginx-deployment
```

pour lister les podes et leurs infos
```
kubectl get po -o wide
kubectl get po --show-labels 
```
et pour les labels

toutes les commandes sur les "get": kubectl get --help
Use "kubectl options" for a list of global command-line options (applies to all commands)

si on change la version de l'image nginx par latest et qu'on recharge avec un apply la nouvelle version est chargée à chaud

on peut taper les commandes en mode impératif 
+ lancement du pod :    kubectl run --image=mmumshad/simple-webapp-color --env="APP_COLOR=red"  --restart=Never simple-webapp-color
+ Suppression du pod :  kubectl delete pod simple-webapp-color
+ Lancement du deploy:  kubectl create deployment --image=nginx:1.18.0 nginx-deployment
+ Scaling du replicas:  kubectl scale --replicas=2 deployment/nginx-deployment
+ Lancement du deploy avec plusieurs replicats:  kubectl create deployment --replicas=2 --image=nginx:1.18.0 nginx-deployment
+ upgrade de version :  kubectl set image deployment/nginx-deployment nginx=nginx

et les recupérer dans un fichier yaml (TIPS)
```
kubectl get deploy nginx-deployment -o yaml > nginx-test.yml
```
pour exposer tous les pods 
```
kubectl port-forward nginx-deployment-59d4b4646d-4zg42  8080:80 --address 0.0.0.0
kubectl port-forward nginx-deployment-59d4b4646d-nmg7x  8081:80 --address 0.0.0.0
```
![port 1](img/k8s.png "port 1") 
![port 2](img/k8s1.png "port 2")
